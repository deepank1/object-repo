

function keys(obj) {

    let key_array = []
    for(let key in obj){
        key_array.push(key.toString());
    }

    return key_array;
}

module.exports = keys;