

function pairs(obj) {
    let pairsArray = [];

    for(let key in obj){
        pairsArray.push([key.toString(),obj[key]]);
    }

    return pairsArray;
}

module.exports = pairs;