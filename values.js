

function values(obj) {
    let values_array = [];

    for (let keys in obj) {
        if (typeof obj[keys] !== "function") {
            values_array.push(obj[keys]);
        }
    }
    return values_array;
}

module.exports = values;