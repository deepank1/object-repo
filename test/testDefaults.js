let obj = {
    name : "Bruce Wayne",
    age : 36,
    location : "Gotham"
}

let defaultProps = {
    age : 53,
    superPower : "Physical and Mental",
    aka : "Dark Knight"
}

let defaults = require("../defaults.js");

let result = defaults(obj, defaultProps);

console.log(result);