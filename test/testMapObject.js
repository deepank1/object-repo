let obj = {
    name : "Bruce Wayne",
    age : 36,
    location : "Gotham"
}

function cb(val,key) {
    return val + 5;
}

let mapObject = require("../mapObject.js");

let result = mapObject(obj,cb);

console.log(result);