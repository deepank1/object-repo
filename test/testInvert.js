let obj = {
    name : "Bruce Wayne",
    age : 36,
    location : "Gotham"
}

let invert = require("../invert.js");

let result = invert(obj);

console.log(result);